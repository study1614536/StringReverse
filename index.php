<?php

declare(strict_types=1);

function stringReverse(string|int $string) {

    $stringArr = mb_str_split((string)$string,1);

    $result = implode('',array_reverse($stringArr));

    return $result;

}
